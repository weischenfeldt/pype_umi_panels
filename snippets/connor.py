import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '24:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('A command-line tool to deduplicate '
                                       'bam files based on custom, '
                                       'inline barcoding'),
                                 add_help=False)


def connor_args(parser, subparsers, argv):
    parser.add_argument('-i', '--in', dest='input',
                        help='Input bam file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output bam file',
                        required=True)
    parser.add_argument('-a', '--annot', dest='annot',
                        help='Annotated bam file with original tags')
    parser.add_argument('-f', '--consensus_freq', dest='cfreq',
                        help='Consensus frequency threshold',
                        type=float, default=0.6)
    parser.add_argument('-s', '--family_size', dest='min_size',
                        help='Minimum family size threshold',
                        type=int, default=3)
    parser.add_argument('-d', '--min_dist', dest='min_dist',
                        help='UMI distance threshold',
                        type=int, default=1)
    return parser.parse_args(argv)


def connor(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = connor_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', program_string(profile.programs['connor']))

    dedup_cmd = ['connor', '--force',
                 '--consensus_freq_threshold', args.cfreq,
                 '--min_family_size_threshold', args.min_size,
                 '--umt_distance_threshold', args.min_dist]
    if args.annot is not None:
        dedup_cmd = dedup_cmd + ['--annotated_output_bam', args.annot]
    dedup_cmd = dedup_cmd + [args.input, args.out]



    log.log.info(' '.join(map(str, dedup_cmd)))
    dedup_cmd = shlex.split(' '.join(map(str, dedup_cmd)))

    log.log.info('Execute connor with python subprocess.Popen')
    dedup_proc = subprocess.Popen(dedup_cmd)
    out0 = dedup_proc.communicate()[0]
    log.log.info('Terminate connor dedup')

    #log.log.info('Creating BAM indexes.')
    #samtools_cmd = ['samtools', 'index', args.out]
    #samtools_proc = subprocess.Popen(samtools_cmd)
    #out1 = samtools_proc.communicate()[0]

