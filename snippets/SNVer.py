import re
import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    return({'vcf': '%s.filter.vcf.gz' % output, 'idx': '%s.filter.vcf.gz.tbi' % output,
        'vcf_raw': '%s.raw.vcf.gz' % output, 'idx_raw': '%s.raw.vcf.gz.tbi' % output,
        'vcf_indel': '%s.indel.filter.vcf.gz' % output,
        'idx_indel': '%s.indel.filter.vcf.gz.tbi' % output,
        'vcf_indel_raw': '%s.indel.raw.vcf.gz' % output,
        'idx_indel_raw': '%s.indel.raw.vcf.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call variant with SNVer',
        add_help=False)


def SNVer_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-f', '--min-allele-freq', dest='min_freq',
                        help='Minimum VAF detected',
                        type=float, default=0.01)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file', required=True)
    return parser.parse_args(argv)


def SNVer(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = SNVer_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java7']))
    module('add', program_string(profile.programs['SNVer']))
    module('add', program_string(profile.programs['tabix']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    snver_dir = os.environ['SNVER_DIR']
    snver_jar = os.path.join(snver_dir, 'SNVerIndividual.jar')
    log.log.info('Use SNVer jar file at %s' % snver_jar)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = '%s' % re.sub('\.gz$', '', args.out)
    output = '%s' % re.sub('\.vcf$', '', output)

    out_vcf_raw = '%s.raw.vcf' % output
    out_indel_raw = '%s.indel.raw.vcf' % output
    out_vcf = '%s.filter.vcf' % output
    out_indel = '%s.indel.filter.vcf' % output

    log.log.info('Preparing snver command line')
    snver_cmd = ['java', '-jar', snver_jar,
                '-b', args.min_freq, '-r', genome,
                '-i', args.input, '-o', output]

    snver_cmd = shlex.split(' '.join(map(str, snver_cmd)))

    log.log.info(' '.join(map(str, snver_cmd)))
    log.log.info('Execute snver with python subprocess.Popen')
    snver_proc = subprocess.Popen(snver_cmd)
    out0 = snver_proc.communicate()[0]

    tabix_vcf(out_vcf, log)
    tabix_vcf(out_indel, log)
    tabix_vcf(out_vcf_raw, log)
    tabix_vcf(out_indel_raw, log)

    log.log.info('Terminate snver')


def tabix_vcf(file_name, log):
    file_name_gz = '%s.gz' % file_name
    bgzip_cmd = ['bgzip', '-f', file_name]
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', file_name_gz]

    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))
    log.log.info('Compress VCF %s to %s' % (file_name, file_name_gz))

    bgzip_proc = subprocess.Popen(bgzip_cmd)
    out0 = bgzip_proc.communicate()[0]

    log.log.info('Index compressed VCF %s' % file_name_gz)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out1 = tabix_proc.communicate()[0]


