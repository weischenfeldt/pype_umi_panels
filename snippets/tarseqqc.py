import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '800:00:00', 'mem': '12gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    except KeyError:
        output = ''
    return({'out': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Allele specific copy number '
                                       'estimation from tumor genomes'),
                                 add_help=False)


def tarseqqc_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input BAM file', required=True)
    parser.add_argument('-b', '--bed', dest='bed',
                        help='BED file defining amplicons and genes regions',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output xlsx report file', required=True)
    parser.add_argument('--mapq', dest='mapq', type=int,
                        help=('Minumum mapQ of alignemt in the QC '
                              'coverage report'),
                        default=60)
    parser.add_argument('--ntq', dest='ntq', type=int,
                        help=('Minumum nucleotide quality in the QC '
                              'coverage report'),
                        default=20)
    parser.add_argument('-d', '--max-depth', dest='max_depth', type=int,
                        help='Maximum depth, larger depths will be truncated',
                        default=10000)
    return parser.parse_args(argv)


def tarseqqc(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = tarseqqc_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['intel_redist']))
    module('add', program_string(profile.programs['intel_compiler']))
    module('add', program_string(profile.programs['R3']))

    genome = profile.files['genome_fa']
    log.log.info('Use genome file %s' % genome)

    out_prefix, out_ext = os.path.splitext(args.out)
    output_xlsx = '%s.xlsx' % out_prefix
    output_plot = '%s.pdf' % out_prefix
    output_amplicons = '%s_amplicons.pdf' % out_prefix
    log.log.info('Output XLSX file to %s' % output_xlsx)

    log.log.info('Prepare R code')

    r_code = '''
    library("TarSeqQC")
    library("BiocParallel")

    ref_fasta <- "%(ref_fasta)s"
    bed_amp_genes <- "%(panel_bed)s"
    bam_file <- "%(input_bam)s"
    checkBedFasta(bed_amp_genes , ref_fasta)
    BPPARAM <- MulticoreParam(2)
    pileupP <- PileupParam(max_depth = %(max_depth)i,
        min_base_quality = %(min_bq)i, min_mapq = %(min_mapq)i)
    myPanel<-TargetExperiment(bed_amp_genes, bam_file,
        ref_fasta, feature = "amplicon", attribute = "coverage",
        BPPARAM = BPPARAM, pileupP = pileupP)
    attributeThres <-c (0,1,50,200,500, Inf)
    pdf("%(image_panel)s")
        plot(myPanel, attributeThres=attributeThres, chrLabels = FALSE)
    dev.off()
    bed <- read.table(bed_amp_genes, header = TRUE, sep = "\\t",
        stringsAsFactors = F)
    pdf("%(amplicon_plots)s", width = 10, height = 7)
        for(i in 1:nrow(bed)) {
            p <- try(plotFeature(myPanel, featureID=bed$name[i],
                BPPARAM=BPPARAM, title = paste("Amp", i, "gene:",
                    bed$gene[i], "chrom:", bed$chr[i] ,sep = " ")))
            if(class(p) == "try-error") next;
            plot(p)
        }
    dev.off()
    buildReport(myPanel, attributeThres=attributeThres, "%(image_panel)s",
        file="%(output)s")
    ''' % {'ref_fasta': genome,
           'panel_bed': args.bed,
           'input_bam': args.input,
           'amplicon_plots': output_amplicons,
           'image_panel': output_plot,
           'max_depth': args.max_depth,
           'output': output_xlsx,
           'min_bq': args.ntq, 'min_mapq': args.mapq}

    log.log.info('Prepare R command line')
    R_cmd = ['R', '--vanilla', '--slave']

    R_cmd = shlex.split(' '.join(map(str, R_cmd)))

    log.log.info(' '.join(map(str, R_cmd)))
    log.log.info('Execute R with python subprocess.Popen')
    tarseqqc_proc = subprocess.Popen(R_cmd, stdin=subprocess.PIPE)
    log.log.info('Pipe R code into R command line: %s'
                 % r_code)
    tarseqqc_proc.stdin.write(r_code)
    tarseqqc_proc.stdin.close()
    out0 = tarseqqc_proc.communicate()[0]

    log.log.info('Terminate tarseqqc')
