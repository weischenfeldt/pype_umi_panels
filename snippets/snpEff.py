import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00', 'mem': '12gb'})


def results(argv):
    try:
        out = argv['--out']
    except KeyError:
        out = argv['-o']
    output = re.sub('.gz$', '', out)
    output = re.sub('.vcf$', '', output)
    out_vcf = '%s.vcf.gz' % output
    out_stats = '%s_summary.html' % output

    return({'out': out_vcf, 'stats': out_stats})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help='Annotate variants with snpEff',
                                 add_help=False)


def snpEff_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output file', required=True)
    return parser.parse_args(argv)


def snpEff(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = snpEff_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['java7']))
    module('add', program_string(profile.programs['snpEff']))
    module('add', program_string(profile.programs['tabix']))

    snpEff_db = profile.files['snpEff_db']
    dbSNP = profile.files['dbSNP']
    cosmicDB = profile.files['cosmic']
    ExAC = profile.files['ExAC']
   
    data_dir, built = os.path.split(snpEff_db)

    log.log.info('Use data dir at %s build %s' % (data_dir, built))
    log.log.info('Use dbSNP database at %s ' % dbSNP)
    log.log.info('Use COSMIC database at %s ' % cosmicDB)
    log.log.info('Use ExAC database at %s ' % ExAC)


    output = re.sub('.gz$', '', args.out)
    output = re.sub('.vcf$', '', output)
    out_stats = '%s_summary.html' % output
    output = '%s.vcf.gz' % output

    snpeff_path = os.environ['SNPEFF_DIR']
    snpsift	= os.path.join(snpeff_path, 'SnpSift.jar')
    
    log.log.info('Use SnpSift jar file at %s' % snpsift)

    snpSift_cmd_dbsnp = ['java', '-jar', snpsift,
                  'annotate', dbSNP, args.input]
    snpSift_cmd_cosmic = ['java', '-jar', snpsift,
                  'annotate', cosmicDB, '-']
    snpSift_cmd_exac = ['java', '-jar', snpsift,
                  'annotate', ExAC, '-']

    snpEff_cmd = ['snpEff', 'ann',
                  built, '-stats', out_stats, '-']
    snpEff_cmd += ['-dataDir', data_dir]
    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output]

    snpSift_cmd_dbsnp = shlex.split(' '.join(map(str, snpSift_cmd_dbsnp)))
    snpSift_cmd_cosmic = shlex.split(' '.join(map(str, snpSift_cmd_cosmic)))
    snpSift_cmd_exac = shlex.split(' '.join(map(str, snpSift_cmd_exac)))
    snpEff_cmd = shlex.split(' '.join(map(str, snpEff_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    with open(output, 'wt') as output_vcf:
        log.log.info('Execute snpEff concat with python subprocess.Popen')
        log.log.info('%s | %s | %s | %s | %s > %s' % (
                                       ' '.join(map(str, snpSift_cmd_dbsnp)),
                                       ' '.join(map(str, snpSift_cmd_cosmic)),
                                       ' '.join(map(str, snpSift_cmd_exac)),
                                       ' '.join(map(str, snpEff_cmd)),
                                       ' '.join(map(str, bgzip_cmd)),
                                       output))
        snpSift_proc1 = subprocess.Popen(snpSift_cmd_dbsnp, stdout=subprocess.PIPE)
        snpSift_proc2 = subprocess.Popen(snpSift_cmd_cosmic, stdin=snpSift_proc1.stdout,
                                         stdout=subprocess.PIPE)
        snpSift_proc3 = subprocess.Popen(snpSift_cmd_exac, stdin=snpSift_proc2.stdout,
                                         stdout=subprocess.PIPE)
        snpEff_proc = subprocess.Popen(snpEff_cmd, stdin=snpSift_proc3.stdout,
                                       stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=snpEff_proc.stdout,
                                      stdout=output_vcf)
        snpSift_proc1.stdout.close()
        snpSift_proc2.stdout.close()
        snpSift_proc3.stdout.close()
        snpEff_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]

    log.log.info('Index output file %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate snpEff')
