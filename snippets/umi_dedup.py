import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '24:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Rmove duplicates using '
                                       'UMI-tools'),
                                 add_help=False)


def umi_dedup_args(parser, subparsers, argv):
    parser.add_argument('--single', dest='single',
                        help='Flag to set single end UMI',
                        action='store_true')
    parser.add_argument('-i', '--in', dest='input',
                        help='Input bam file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output bam file',
                        required=True)
    parser.add_argument('-s', '--stats', dest='stats',
                        help='Output stats file')
    parser.add_argument('-m', '--method', dest='method',
                        help=('Method used to identify PCR '
                              'duplicates within reads'),
                        choices=['unique', 'percentile', 'cluster',
                                 'adjacency', 'directional'],
                        default='directional')
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/scratch')
    return parser.parse_args(argv)


def umi_dedup(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = umi_dedup_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['umi_tools']))
    module('add', program_string(profile.programs['samtools_1']))

    dedup_cmd = ['umi_tools', 'dedup', '--method', args.method,
                 '-I', args.input, '-S', args.out]

    if not args.single:
        dedup_cmd = dedup_cmd + ['--paired']

    if args.stats:
        dedup_cmd = dedup_cmd + ['--output-stats', args.stats]



    log.log.info(' '.join(map(str, dedup_cmd)))
    dedup_cmd = shlex.split(' '.join(map(str, dedup_cmd)))

    log.log.info('Execute umi_tools dedup with python subprocess.Popen')
    dedup_proc = subprocess.Popen(dedup_cmd)
    out0 = dedup_proc.communicate()[0]
    log.log.info('Terminate umi_tools dedup')

    log.log.info('Creating BAM indexes.')
    samtools_cmd = ['samtools', 'index', args.out]
    samtools_proc = subprocess.Popen(samtools_cmd)
    out1 = samtools_proc.communicate()[0]

