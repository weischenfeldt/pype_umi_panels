import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '24:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Extract UMI from fastq using '
                                       'UMI-tools'),
                                 add_help=False)

def umi_extract_args(parser, subparsers, argv):
    parser.add_argument('--in1', dest='input1',
                        help='Input fastq file',
                        required=True)
    parser.add_argument('--out1', dest='out1',
                        help='Output fastq file',
                        required=True)
    parser.add_argument('--bc-pattern', dest='bc1',
                        help='UMI pattern', type=str,
                        required=True)
    parser.add_argument('--in2', dest='input2',
                        help='Input mate-pair fastq file')
    parser.add_argument('--out2', dest='out2',
                        help='Output mate-pair fastq file')
    parser.add_argument('--bc-pattern2', dest='bc2',
                        help='UMI pattern in the mate fastq',
                        type=str)

    return parser.parse_args(argv)


def umi_extract(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = umi_extract_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', program_string(profile.programs['umi_tools']))

    extract_cmd = ['umi_tools', 'extract', '--bc-pattern', args.bc1,
                 '-I', args.input1, '-S', args.out1]

    if  args.input2 is None or args.out2 is None or args.bc2 is None:
        if args.input2 == args.out2 == args.bc2:
            pass
        else:
            raise Exception(('If paired-end all arguments --in2, --ont2'
                             ' and --bc-pattern2 must be set'))
    else:
        extract_cmd = extract_cmd + ['--read2-in', args.input2,
            '--read2-out', args.out2, '--bc-pattern2', args.bc2]

    log.log.info(' '.join(map(str, extract_cmd)))
    extract_cmd = shlex.split(' '.join(map(str, extract_cmd)))

    log.log.info('Execute umi_tools extract with python subprocess.Popen')
    extract_proc = subprocess.Popen(extract_cmd)
    out0 = extract_proc.communicate()[0]
    log.log.info('Terminate umi_tools extract')
