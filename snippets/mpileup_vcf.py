import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        out = argv['--out']
    except KeyError:
        out = argv['-o']
    output = re.sub('.gz$', '', out)
    output = re.sub('.vcf$', '', output)
    out_vcf = '%s.vcf.gz' % output
    return({'vcf': out_vcf, 'tbi': '%s.tbi' % out_vcf})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help='Variant calls with samtools',
                                 add_help=False)


def mpileup_vcf_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='List of input BAM files', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file',
                        required=True)
    parser.add_argument('-t', '--tags', dest='tags', type=str,
                        help=('Comma separated optional tags'
                              ' to output'),
                        default='DP,DPR,DV,DP4')
    parser.add_argument('-d', '--max-depth', dest='depth', type=int,
                        help='Maximum depth, default 250',
                        default=250)
    return parser.parse_args(argv)


def mpileup_vcf(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = mpileup_vcf_args(add_parser(subparsers, module_name),
                            subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['tabix']))

    genome = profile.files['genome_fa']
    log.log.info('Use genome file %s' % genome)

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.vcf$', '', output)
    out_stats = '%s_summary.html' % output
    output = '%s.vcf.gz' % output

    log.log.info('Preparing samtools command line')
    mpileup_cmd = ['samtools', 'mpileup', '-f', genome,
                   '--max-depth', args.depth, '-v', '-t',
                   args.tags, '-I', '-o', output]
    for input_bam in args.input:
        mpileup_cmd.append(input_bam)

    log.log.info(' '.join(map(str, mpileup_cmd)))
    mpileup_cmd = shlex.split(' '.join(map(str, mpileup_cmd)))
    log.log.info('Execute samtools mpileup with python subprocess.Popen')
    mpileup_proc = subprocess.Popen(mpileup_cmd, stdout=subprocess.PIPE)
    out0 = mpileup_proc.communicate()[0]

    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output]
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info('Index output file %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out1 = tabix_proc.communicate()[0]

    log.log.info('Terminate samtools mpileup')
