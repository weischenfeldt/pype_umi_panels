import os
import shlex
import subprocess

from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help='Merge BAMs with samtools',
                                 add_help=False)


def samtools_merge_args(parser, subparsers, argv):
    parser.add_argument('-b', '--bam', dest='bams', nargs='*',
                        help='List of BAM files to merge', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output name for the merged BAM file',
                        required=True)
    return parser.parse_args(argv)


def samtools_merge(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = samtools_merge_args(add_parser(subparsers, module_name),
                                subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['samtools_1']))

    log.log.info('Preparing samtools2 bammarkduplicates2  command line')
    random_str = generate_uid()
    merge_cmd = ['samtools', 'merge', '-f', args.out]
    for input in args.bams:
        merge_cmd.append(input)

    log.log.info(' '.join(map(str, merge_cmd)))
    merge_cmd = shlex.split(' '.join(map(str, merge_cmd)))
    log.log.info('Execute samtools merge with python subprocess.Popen')
    merge_proc = subprocess.Popen(merge_cmd, stdout=subprocess.PIPE)
    out0 = merge_proc.communicate()[0]

    log.log.info('Creating BAM indexes.')
    samtools_cmd = ['samtools', 'index', args.out]
    samtools_proc = subprocess.Popen(samtools_cmd)
    out1 = samtools_proc.communicate()[0]

    log.log.info('Terminate samtools merge')

