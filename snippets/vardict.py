import re
import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    output = '%s' % re.sub('.txt$', '', output)
    return({'vcf': '%s.vcf.gz' % output, 'idx': '%s.vcf.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call variant with VarDictJava',
        add_help=False)


def vardict_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-f', '--min-allele-freq', dest='min_freq',
                        help='Minimum VAF detected',
                        type=float, default=0.01)
    parser.add_argument('-s', '--sample-name', dest='name',
                        help='Sample name, by default the input file name is used')
    parser.add_argument('-b', '--target-bed', dest='target',
                        help='Bed file with target region',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output txt file', required=True)
    return parser.parse_args(argv)


def vardict(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = vardict_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java8']))
    module('add', program_string(profile.programs['VarDict']))
    module('add', program_string(profile.programs['tabix']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = '%s' % re.sub('\.gz$', '', args.out)
    output = '%s' % re.sub('\.vcf$', '', output)
    output = '%s' % re.sub('\.txt$', '', output)

    output = '%s.vcf.gz' % re.sub('\.vcf$', '', output)

    log.log.info('Preparing VarDict command line')

    # vardict -C -G Ref GRCh37.67.fasta -f $AF THR -N sampleX -b sampleX.bam -h -c 1 -S 2 \
    # -E 3 -g 4 target.bed > sampleX.txt

    vardict_cmd = ['VarDict', '-C', '-f', args.min_freq,
                '-h', '-c', 1, '-G', genome, '-Q', args.min_qual,
                '-S', 2, '-E', 3, '-g', 4, '-O', args.min_map,
                '-b', args.input, '-V', args.min_freq]
    if args.name is not None:
        vardict_cmd += ['-N', args.name]

    if args.target is not None:
        vardict_cmd += [args.target]

    var2vcf_cmd = ['var2vcf_valid.pl', '-f', args.min_freq]
    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', output]

    vardict_cmd = shlex.split(' '.join(map(str, vardict_cmd)))
    var2vcf_cmd = shlex.split(' '.join(map(str, var2vcf_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info(' '.join(map(str, vardict_cmd)))
    log.log.info('Open file %s in writing mode' % output)
    with open(output, 'wt') as output_file:
        log.log.info('Execute vardict with python subprocess.Popen')
        vardict_proc = subprocess.Popen(vardict_cmd, stdout=subprocess.PIPE)
        var2vcf_proc = subprocess.Popen(var2vcf_cmd, stdin=vardict_proc.stdout,
                                      stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=var2vcf_proc.stdout,
                                      stdout=output_file)
        var2vcf_proc.stdout.close()
        vardict_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]
    log.log.info('Index compressed VCF %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate vardict')
