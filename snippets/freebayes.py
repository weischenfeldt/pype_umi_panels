import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    try:
        region = argv['--region']
    except KeyError:
        region = None
    try:
        region = argv['-r']
    except KeyError:
        region = None
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    if region:
        readable_reg = re.sub(':', '_', region)
        readable_reg = re.sub('-', '_', readable_reg)
        readable_reg = re.sub('\.\.', '_', readable_reg)
        output = '%s_%s' % (output, readable_reg)
    return({'vcf': '%s.vcf.gz' % output, 'idx': '%s.vcf.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call variant with freebayes', add_help=False)


def freebayes_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='input bam files', required=True)
    parser.add_argument('-r', '--region', dest='region',
                        help=('Limit analysis to the specified region, '
                              '0-base coordinates, end_position not included '
                              '(same as BED format). Either \'-\' or \'..\' '
                              'can be used as a separator.'))
    parser.add_argument('-C', '--min-alternate-count', dest='min_altn',
                        help=('Require at least this count of observations '
                              'supporting an alternate allele'),
                        type=int, default=1)
    parser.add_argument('-F', '--min-alternate-fraction', dest='min_frac',
                        help=('Require at least this fraction of '
                              'observations supporting an alternate allele'),
                        type=float, default=0.01)
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file', required=True)
    return parser.parse_args(argv)


def freebayes(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = freebayes_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['freebayes']))
    module('add', program_string(profile.programs['tabix']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = '%s' % re.sub('\.gz$', '', args.out)
    output = '%s' % re.sub('\.vcf$', '', output)
    if args.region:
        readable_reg = re.sub(':', '_', args.region)
        readable_reg = re.sub('-', '_', readable_reg)
        readable_reg = re.sub('\.\.', '_', readable_reg)
        output = '%s_%s' % (output, readable_reg)

    output = '%s.vcf.gz' % re.sub('\.vcf$', '', output)

    log.log.info('Preparing freebayes command line')
    freebayes_cmd = ['freebayes', '--fasta-reference', genome,
                     '-X', '-u', '-F', args.min_frac, '-C', args.min_altn,
                     '-m', args.min_map, '-q', args.min_qual]
    if args.region:
        freebayes_cmd += ['--region', args.region]
    freebayes_cmd += args.input

    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output]

    freebayes_cmd = shlex.split(' '.join(map(str, freebayes_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info(' '.join(map(str, freebayes_cmd)))
    log.log.info('Open file %s in writing mode' % output)
    with open(output, 'wt') as output_file:
        log.log.info('Execute freebayes with python subprocess.Popen')
        freebayes_proc = subprocess.Popen(freebayes_cmd,
                                          stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=freebayes_proc.stdout,
                                      stdout=output_file)
        freebayes_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]
    log.log.info('Index compressed VCF %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate freebayes')
