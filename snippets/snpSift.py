import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        out = argv['--out']
    except KeyError:
        out = argv['-o']
    output = re.sub('.gz$', '', out)
    output = re.sub('.vcf$', '', output)
    out_vcf = '%s.vcf.gz' % output

    return({'out': out_vcf, 'idx': '%s.tbi' % out_vcf})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help='Filter variants with snpSift',
                                 add_help=False)


def snpSift_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output file', required=True)
    parser.add_argument('-f', '--filter', dest='filter',
                        help='snpSift Filter string',
                        default='isHet( GEN[*] ) & isVariant( GEN[*] )')
    return parser.parse_args(argv)


def snpSift(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = snpSift_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java7']))
    module('add', program_string(profile.programs['snpEff']))
    module('add', program_string(profile.programs['tabix']))

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.vcf$', '', output)
    output = '%s.vcf.gz' % output

    snpeff_path = os.environ['SNPEFF_DIR']
    snpsift = os.path.join(snpeff_path, 'SnpSift.jar')

    log.log.info('Use SnpSift jar file at %s' % snpsift)

    snpSift_filter = ['java', '-jar', snpsift,
                      'filter', '"%s"' % args.filter, args.input]

    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output]

    snpSift_filter = shlex.split(' '.join(map(str, snpSift_filter)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    with open(output, 'wt') as output_vcf:
        log.log.info('Execute snpSift concat with python subprocess.Popen')
        log.log.info('%s | %s > %s' % (
            ' '.join(map(str, snpSift_filter)),
            ' '.join(map(str, bgzip_cmd)),
            output))
        snpSift_proc = subprocess.Popen(snpSift_filter, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=snpSift_proc.stdout,
                                      stdout=output_vcf)
        snpSift_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]

    log.log.info('Index output file %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate snpSift')
